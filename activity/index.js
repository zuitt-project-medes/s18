function Pokemon(name, level, hp){
    // properties
        this.name = name;
        this.level = level;
        this.health = hp * 2;
        this.attack = level;

        // methods
        this.tackle = function(target) {
        target.health -= this.attack;
        console.log(this.name + ' tackled '+ target.name);
        console.log(target.name + "'s health is now reduced to " + (target.health));

            if(target.health <= 10) {
            target.faint()
            }
        },

        this.faint = function(){
            console.log(`${this.name} fainted`)
        }
}

// create new instances of the Pokemon object each with their unique properties

let charizard = new Pokemon("Charizard", 25, 100);
let raichu = new Pokemon("Raichu", 20, 80);

charizard.tackle(raichu);
raichu.tackle(charizard);
charizard.tackle(raichu);
raichu.tackle(charizard);

charizard.tackle(raichu);
charizard.tackle(raichu);
charizard.tackle(raichu);
charizard.tackle(raichu);
