// alert("hi")

// a variable with multiple values

// a:array
let cartoons = ["Elmo", "Mickey", "Daisy", "Barbie"]

// javascript objects

// similar to an array, it also contains multiple values''unlike an array that uses index, objects use properties

// with object, we can easily ive labels to each value

/*

Structure of Syntax:

letobjectName = {
	key : value
	key : value (function)
	key : object{
		key : value
	},
	key : [array]
}

*/


let cellphone = {
	name: "Nokia 3210", 
	manufactureDate: "1999"
};

console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor function

/*
Syntax:

function objectName(keyA, keyB){
	this.keyA = keyA;
	this.keyB = keyB;
};
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.releaseDate = manufactureDate;
};

let laptop = new Laptop("Lenovo", 2008);
console.log(laptop);
// will create new object
// new operator creates an instance of an object

let laptop1 = Laptop("Asus", 2010);
console.log(laptop1);
// result: undefined

// creating empty objects

let computer = {}
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

// accessing array objects

let machines = [laptop, laptop1];

// access the property of an object inside an array
console.log(machines[0].name);
// result: Lenovo

// another way of accessing properties in an array
console.log(machines[0]["releaseDate"]);
// result: 2008

// initializing/ adding/ deleting/ reassigning object properties

let car = {};

// initializing/ adding object properties

car.name = "Honda Civic";
console.log(car);

// adding object properties with square brackets

car["manufactureDate"] = 2019;
console.log(car);

// deleting object properties
delete car["name"];
console.log(car);

/*
Mini-Activity

1. Add properties tp the object car:
	a. name
	b. model
	c. color
	d. location

*/

car.brand = "Honda";
car.model = "Civic";
car.color = "Pearl White";
car.location = "QC";
console.log(car);

// reassigning object properties

car.manufactureDate = 1986
console.log(car);


// objet methods

// a metgid in an object is a function which is a property of an object
// they are also functions and one of the key differences they have is that methods are functions related to a specific object

// methods are defined based on what an object is capable of doing and how it should work

let person = {
	name: "Jin",
	talk: function(){
		console.log(`Hello my name is ${this.name}`);

	}
};
person.talk();

let friend = {
	firstName: "Bam",
	lastName: "Jeon",
	address: {
		city: "Caloocan",
		country: "Philippines",
	},
	emails: ["bam@mail.com", "jeonbam@gmail.com"],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}, I live in ${this.address.city}. My personal email is ${this.emails[0]}`)
	}
}
friend.introduce();

// real world application

// scenario:

// 1. create a game that would have several pokemons to interact with each other
// 2. every pokemon should have stats, properies, functions

let myPokemon = {
	name: "Bulbasaur", 
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(`${this.name} tackled another pokemon`)
		console.log(`targetPokemon's health is now reduced`)
	},
	faint: function(){
		console.log(`${this.name} fainted`);
	}
};


myPokemon.tackle();
myPokemon.faint();

// creating object with an object constructor

function Pokemon(name, lvl, hp){
	// properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		// methods
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`)
			console.log(`targetPokemon's health is now reduced`);
			console.log(this.name);
		}
		this.faint = function(){
			console.log(`${this.name} fainted`)
		}
}

// create new instances of the Pokemon object each with their unique properties

let pikachu = new Pokemon("Pikachu", 3, 50);
let ratata = new Pokemon("Ratata", 5, 25);

// Providing the "ratata" object as an argument to "pikachu" tackle methood will create interaction between the two objects

pikachu.tackle(ratata);
ratata.tackle(pikachu);



















































